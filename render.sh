#!/bin/bash

if [[ -z "${CI_REGISTRY_IMAGE}" ]]; then
    export CI_REGISTRY_IMAGE="localhost:32000"
fi
if [[ -z "${REGISTRY_ADDRESS}" ]]; then
    export REGISTRY_ADDRESS="localhost:32000"
fi
if [[ -z "${DEPLOYMENT_ENVIRONMENT}" ]]; then
    export DEPLOYMENT_ENVIRONMENT="development"
fi

python render-templates.py -i templates -o manifests
python render-templates.py -i helm/src -o helm/build -w values.yaml
